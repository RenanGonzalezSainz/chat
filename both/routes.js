Router.configure({
    layoutTemplate: 'Home1'
});

Router.route('/', function () {
    this.render('Welcome');
}, {
    name: 'Welcome'
});

Router.route('/main', function () {
    this.render('main');
}, {
    name: 'main'
});

Router.route('/profile', function () {
    this.render('profile');
}, {
    name: 'profile'
});

Router.route('/chat', function () {
    this.render('chat');
}, {
    name: 'chat'
});
//
//Router.route('/atPwdFormBtn', function () {
//    this.render('atPwdFormBtn');
//}, {
//    name: 'atPwdFormBtn'
//});